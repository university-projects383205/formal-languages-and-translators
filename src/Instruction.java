public class Instruction {
  private OpCode opCode;
  private int operand;

  public Instruction(OpCode opCode) {
    this.opCode = opCode;
  }

  public Instruction(OpCode opCode, int operand) {
    this.opCode = opCode;
    this.operand = operand;
  }

  public String toJasmin() {
    String temp = "";

    switch (opCode) {
      case ldc: temp = " ldc " + this.operand + "\n"; break;
      case iadd: temp = " iadd " + "\n"; break;
      case invokestatic: if (this.operand == 1)  temp = " invokestatic " + "Output/print(I)V" + "\n";
                          else temp = " invokestatic " + "Output/read()I" + "\n"; break;
      case imul: temp = " imul " + "\n"; break;
      case idiv: temp = " idiv " + "\n"; break;
      case isub: temp = " isub " + "\n"; break;
      case istore: temp = " istore " + this.operand + "\n"; break;
      case iload: temp = " iload " + this.operand + "\n"; break;
      case if_icmpeq: temp = " if_icmpeq L" + this.operand + "\n"; break;
      case if_icmple: temp = " if_icmple L" + this.operand + "\n"; break;
      case if_icmplt: temp = " if_icmplt L" + this.operand + "\n"; break;
      case if_icmpne: temp = " if_icmpne L" + this.operand + "\n"; break;
      case if_icmpge: temp = " if_icmpge L" + this.operand + "\n"; break;
      case if_icmpgt: temp = " if_icmpgt L" + this.operand + "\n"; break;
      case ifne: temp = " ifne L" + this.operand + "\n"; break;
      case GOto: temp = " goto L" + this.operand + "\n" ; break;
      case label: temp = "L" + this.operand + ":\n"; break;
    }

    return temp;
  }
}
