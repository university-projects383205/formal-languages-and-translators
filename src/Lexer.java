import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Lexer {
  public int line = 1;
  public int character = 0;
  private char peek = ' ';

  private void readch(BufferedReader br) {
    try {
      this.peek = (char) br.read();
      this.character++;
    } catch (IOException exc) {
      // ERROR
      this.peek = (char) -1;
    }
  }

  public Token lexical_scan(BufferedReader br) {
    while (this.peek == ' ' || this.peek == '\t' || this.peek == '\n'  || this.peek == '\r') {
      if (this.peek == '\n') {
        this.line++;
        this.character = 0;
      }

      readch(br);
    }

    // System.out.println("[*] Line: " + this.line);
    // System.out.println("[*] Character: " + this.character);
    // System.out.println("[*] Peek: " + this.peek);
    // System.out.println();

    switch (this.peek) {
      case '!':
        this.peek = ' ';
        return Token.not;

      case '(':
        this.peek = ' ';
        return Token.lpt;

      case ')':
        this.peek = ' ';
        return Token.rpt;

      case '{':
        this.peek = ' ';
        return Token.lpg;

      case '}':
        this.peek = ' ';
        return Token.rpg;

      case '+':
        this.peek = ' ';
        return Token.plus;

      case '-':
        this.peek = ' ';
        return Token.minus;

      case '*':
        this.peek = ' ';
        return Token.mult;

      case '/':
        readch(br);

        if (this.peek == '*') {
          boolean is_commented = true;
          boolean missing_closing_marker = false;
          int state = 2;

          while (is_commented) {
            do {
              readch(br);

              if (this.peek == (char) -1) {
                is_commented = false;
                missing_closing_marker = true;
                this.line--;
                this.character--;
              }
              else if (this.peek == '\n') {
                this.line++;
              }
            }
            while (this.peek != '*' && state == 2 && is_commented);

            state = 3;

            while (this.peek == '*' && state != 2) {
              readch(br);

              if (this.peek == '\n') {
                this.line++;
              }
            }

            if (this.peek == '/') {
              is_commented = false;
              this.peek = ' ';
            }
            else {
              state = 2;
            }
          }

          if (missing_closing_marker) {
            System.err.println("[" + this.line + ":" + this.character + "] Missing closing marker '*/' for comment block");
            return null;
          }
          else {
            // Rieseguo lexical_scan per continuare a fare l'analisi lessicale da dopo il commento
            return lexical_scan(br);
          }
        }
        else if (this.peek == '/') {
          while (this.peek != '\n' && this.peek != (char) -1) {
            readch(br);
          }

          return lexical_scan(br);
        }
        else {
          return Token.div;
        }

      case ';':
        this.peek = ' ';
        return Token.semicolon;

      case '&':
        readch(br);

        if (this.peek == '&') {
          this.peek = ' ';
          return Word.and;
        }
        else {
          System.err.println("[" + this.line + ":" + this.character + "] Erroneous character after '&' : "  + this.peek);
          return null;
        }

      case '|':
        readch(br);

        if (this.peek == '|') {
          this.peek = ' ';
          return Word.or;
        }
        else {
          System.err.println("[" + this.line + ":" + this.character + "] Erroneous character after '|' : "  + this.peek);
          return null;
        }

      case '<':
        readch(br);

        if (this.peek == '=') {
          this.peek = ' ';
          return Word.le;
        }
        else if (this.peek == '>') {
          this.peek = ' ';
          return Word.ne;
        }
        else {
          return Word.lt;
        }

      case '>':
        readch(br);

        if (this.peek == '=') {
          this.peek = ' ';
          return Word.ge;
        }
        else {
          return Word.gt;
        }

      case '=':
        readch(br);

        if (this.peek == '=') {
          this.peek = ' ';
          return Word.eq;
        }
        else {
          System.err.println("[" + this.line + ":" + this.character + "] Erroneous character after '=' : "  + this.peek);
          return null;
        }

      case ':':
        readch(br);

        if (this.peek == '=') {
          this.peek = ' ';
          return Word.assign;
        }
        else {
          System.err.println("[" + this.line + ":" + this.character + "] Erroneous character after ':' : "  + this.peek);
          return null;
        }

      case (char) -1:
        return new Token(Tag.EOF);

      default:
        if (Character.isLetter(this.peek) || this.peek == '_') {
          String read_word = "";

          while (this.peek == '_' || Character.isDigit(this.peek) || Character.isLetter(this.peek)) {
            read_word += this.peek;
            readch(br);
          }

          Word[] reserved_words = {Word.casetok, Word.when, Word.then, Word.elsetok, Word.whiletok, Word.dotok, Word.print, Word.read};
          boolean is_reserved = false;

          int i = 0;
          while (i < reserved_words.length && !is_reserved) {
            is_reserved = (read_word.equals(reserved_words[i].lexeme));

            if (!is_reserved) {
              i++;
            }
          }

          if (is_reserved) {
            return reserved_words[i];
          }
          else {
            boolean only_underscores = true;

            for (int j = 0; j < read_word.length() && only_underscores; j++) {
              only_underscores = (read_word.charAt(j) == '_');
            }

            if (!only_underscores) {
              return new Word(Tag.ID, read_word);
            }
            else {
              System.err.println("[" + this.line + ":" + this.character + "] Found ID composed only by underscores: " + read_word);
              return null;
            }
          }
        }
        else if (Character.isDigit(this.peek)) {
          String read_word = "";

          if (this.peek == '0') {
            read_word += this.peek;
            readch(br);

            if (Character.isDigit(this.peek)) {
              System.err.println("[" + this.line + ":" + this.character + "] Erroneous character: '" + this.peek + "'");
              return null;
            }
            else {
              return new NumberTok(read_word);
            }
          }
          else {
            while (Character.isDigit(this.peek)) {
              read_word += this.peek;
              readch(br);
            }

            return new NumberTok(read_word);
          }
        }
        else {
          System.err.println("[" + this.line + ":" + this.character + "] Erroneous character: " + this.peek);
          return null;
        }
    }
  }

  public static void main(String[] args) {
    Lexer lex = new Lexer();
    String path = args[0];

    try {
      BufferedReader br = new BufferedReader(new FileReader(path));
      Token tok;

      do {
        tok = lex.lexical_scan(br);
        System.out.println("Scan: " + tok);
      } while (tok.tag != Tag.EOF);

      br.close();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
