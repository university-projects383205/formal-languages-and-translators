public class NumberTok extends Token {
  protected int lexeme;

  public NumberTok(String num) {
    super(Tag.NUM);
    this.lexeme = Integer.valueOf(num);
  }

  public String toString() {
    return "< " + tag + ", " + lexeme + " >";
  }
}
