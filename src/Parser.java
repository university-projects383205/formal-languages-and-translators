import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Parser {
  private Lexer lex;
  private BufferedReader pbr;
  private Token look;

  public Parser(Lexer l, BufferedReader br) {
    lex = l;
    pbr = br;
    move();
  }

  private void move() {
    look = lex.lexical_scan(pbr);
    System.out.println("Token: " + look);
  }

  private void error(String s) {
    throw new Error("Near line " + lex.line + ": " + s);
  }

  private void match(int t) {
    if (look.tag == t) {
      if (look.tag != Tag.EOF) {
        move();
      }
    }
    else {
      error("[!] Syntax error: expected '" + (char) t + "', found " + look + " instead");
    }
  }

  public void prog() {
    switch (look.tag) {
      case '{':
      case Tag.ID:
      case Tag.PRINT:
      case Tag.READ:
      case Tag.CASE:
      case Tag.WHILE:
        statlist();
        match(Tag.EOF);
        break;

      default:
        error("[!] Error occured during execution of prog()");
        break;
    }
  }

  private void statlist() {
    switch (look.tag) {
      case '{':
      case Tag.ID:
      case Tag.PRINT:
      case Tag.READ:
      case Tag.CASE:
      case Tag.WHILE:
        stat();
        statlistp();
        break;

      default:
        error("[!] Error occured during execution of statlist()");
        break;
    }
  }

  private void statlistp() {
    switch (look.tag) {
      case ';':
        match(';');
        stat();
        statlistp();
        break;

      case '}':
      case Tag.EOF:
        break;

      default:
        error("[!] Error occured during execution of statlistp()");
        break;
    }
  }

  private void stat() {
    switch (look.tag) {
      case '{':
        match('{');
        statlist();
        match('}');
        break;

      case Tag.ID:
        match(Tag.ID);
        match(Tag.ASSIGN);
        expr();
        break;

      case Tag.PRINT:
        match(Tag.PRINT);
        match('(');
        expr();
        match(')');
        break;

      case Tag.READ:
        match(Tag.READ);
        match('(');
        match(Tag.ID);
        match(')');
        break;

      case Tag.CASE:
        match(Tag.CASE);
        whenlist();
        match(Tag.ELSE);
        stat();
        break;

      case Tag.WHILE:
        match(Tag.WHILE);
        match('(');
        bexpr();
        match(')');
        stat();
        break;

      default:
        error("[!] Error occured during execution of stat()");
        break;
    }
  }

  private void whenlist() {
    switch (look.tag) {
      case Tag.WHEN:
        whenitem();
        whenlistp();
        break;

      default:
        error("[!] Error occured during execution of whenlist()");
        break;
    }
  }

  private void whenlistp() {
    switch (look.tag) {
      case Tag.WHEN:
        whenitem();
        whenlistp();
        break;

      case Tag.ELSE:
        break;

      default:
        error("[!] Error occured during execution of whenlistp()");
        break;
    }
  }

  private void whenitem() {
    switch (look.tag) {
      case Tag.WHEN:
        match(Tag.WHEN);
        match('(');
        bexpr();
        match(')');
        stat();
        break;

      default:
        error("[!] Error occured during execution of whenitem()");
        break;
    }
  }

  private void bexpr() {
    switch (look.tag) {
      case '(':
      case Tag.NUM:
      case Tag.ID:
        expr();
        match(Tag.RELOP);
        expr();
        break;

      default:
        error("[!] Error occured during execution of bexpr()");
        break;
    }
  }

  private void expr() {
    switch (look.tag) {
      case '(':
      case Tag.NUM:
      case Tag.ID:
        term();
        exprp();
        break;

      default:
        error("[!] Error occured during execution of expr()");
        break;
    }
  }

  private void exprp() {
    switch (look.tag) {
      case '+':
      case '-':
        match(look.tag);
        term();
        exprp();
        break;

      case ';':
      case '}':
      case ')':
      case Tag.WHEN:
      case Tag.ELSE:
      case Tag.RELOP:
      case Tag.EOF:
        break;

      default:
        error("[!] Error occured during execution of exprp()");
        break;
    }
  }

  private void term() {
    switch (look.tag) {
      case '(':
      case Tag.NUM:
      case Tag.ID:
        fact();
        termp();
        break;

      default:
        error("[!] Error occured during execution of term()");
        break;
    }
  }

  private void termp() {
    switch (look.tag) {
      case '*':
      case '/':
        match(look.tag);
        fact();
        termp();
        break;

      case '+':
      case '-':
      case ';':
      case '}':
      case ')':
      case Tag.WHEN:
      case Tag.ELSE:
      case Tag.RELOP:
      case Tag.EOF:
        break;

      default:
        error("[!] Error occured during execution of termp()");
        break;
    }
  }

  private void fact() {
    switch (look.tag) {
      case '(':
        match('(');
        expr();
        match(')');
        break;

      case Tag.NUM:
        match(Tag.NUM);
        break;

      case Tag.ID:
        match(Tag.ID);
        break;

      default:
        error("[!] Error occured during execution of fact()");
        break;
    }
  }

  public static void main(String[] args) {
    Lexer lex = new Lexer();
    String path = args[0];

    try {
      BufferedReader br = new BufferedReader(new FileReader(path));
      Parser parser = new Parser(lex, br);

      parser.prog();
      System.out.println("Input OK");
      br.close();
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
