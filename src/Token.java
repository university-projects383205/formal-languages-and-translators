public class Token {
    public final int tag;
    public static final Token not = new Token('!'),
      lpt = new Token('('),
      rpt = new Token(')'),
      lpg = new Token('{'),
      rpg = new Token('}'),
      plus = new Token('+'),
      minus = new Token('-'),
      mult = new Token('*'),
      div = new Token('/'),
      semicolon = new Token(';');

    public Token(int t) {
      tag = t;
    }

    public String toString() {
      if (tag == -1) {
        return "< " + tag + ", EOF >";
      }
      else {
        return "< " + tag + ", " + (char) tag + " >";
      }
    }
}
