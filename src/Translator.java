import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Translator {
  private Lexer lex;
  private BufferedReader pbr;
  private Token look;

  private SymbolTable st = new SymbolTable();
  private CodeGenerator code = new CodeGenerator();
  private int count = 0;

  public Translator(Lexer l, BufferedReader br) {
    lex = l;
    pbr = br;
    move();
  }

  private void move() {
    look = lex.lexical_scan(pbr);
    System.out.println("Token: " + look);
  }

  private void error(String s) {
    throw new Error("Near line " + lex.line + ": " + s);
  }

  private void match(int t) {
    if (look.tag == t) {
      if (look.tag != Tag.EOF) {
        move();
      }
    }
    else {
      error("[!] Syntax error: expected '" + (char) t + "', found " + look + " instead");
    }
  }

  public void prog() {
    switch (look.tag) {
      case '{':
      case Tag.ID:
      case Tag.PRINT:
      case Tag.READ:
      case Tag.CASE:
      case Tag.WHILE:
        int lnext_prog = code.newLabel();
        statlist(lnext_prog);
        code.emitLabel(lnext_prog);
        match(Tag.EOF);

        try {
          code.toJasmin();
        }
        catch (IOException ex) {
          ex.printStackTrace();
          // System.out.println("[!] IO error");
        };
        break;

      default:
        error("[!] Error occured during execution of prog() with token: " + look);
        break;
    }
  }

  public void statlist(int lnext) {
    switch (look.tag) {
      case '{':
      case Tag.ID:
      case Tag.PRINT:
      case Tag.READ:
      case Tag.CASE:
      case Tag.WHILE:
        int lnext_stat = code.newLabel();
        stat(lnext_stat);
        code.emitLabel(lnext_stat);
        statlistp(lnext);
        break;

      default:
        error("[!] Error occured during execution of statlist() with token: " + look);
        break;
    }
  }

  public void statlistp(int lnext) {
    switch (look.tag) {
      case ';':
        match(';');
        int lnext_stat = code.newLabel();
        stat(lnext_stat);
        code.emitLabel(lnext_stat);
        statlistp(lnext);
        break;

      case '}':
      case Tag.EOF:
        break;

      default:
        error("[!] Error occured during execution of statlistp() with token: " + look);
        break;
    }
  }

  public void stat(int lnext) {
    switch(look.tag) {
      case '{':
        match('{');
        int sl_next = lnext;
        statlist(sl_next);
        match('}');
        break;

      case Tag.ID:
        String id = ((Word) look).lexeme;
        int id_addr = st.lookupAddress(id);
        if (id_addr == -1) {
          id_addr = count;
          st.insert(id, count++);
        }

        match(Tag.ID);
        match(Tag.ASSIGN);
        expr();
        code.emit(OpCode.istore, st.lookupAddress(id));
        break;

      case Tag.PRINT:
        match(Tag.PRINT);
        match('(');
        expr();
        code.emit(OpCode.invokestatic, 1);
        match(')');
        break;

      case Tag.READ:
        match(Tag.READ);
        match('(');
        if (look.tag == Tag.ID) {
          int read_id_addr = st.lookupAddress(((Word) look).lexeme);
          if (read_id_addr == -1) {
            read_id_addr = count;
            st.insert(((Word) look).lexeme, count++);
          }

          match(Tag.ID);
          match(')');
          code.emit(OpCode.invokestatic, 0);
          code.emit(OpCode.istore, read_id_addr);
        }
        else {
          error("Error in grammar (stat) after read( with " + look);
        }
        break;

      case Tag.CASE:
        match(Tag.CASE);
        whenlist(lnext);
        match(Tag.ELSE);
        stat(lnext);
        break;

      case Tag.WHILE:
        match(Tag.WHILE);
        match('(');
        int ltrue_while = code.newLabel();
        int lfalse_while = lnext;
        int lbegin = code.newLabel();
        code.emitLabel(lbegin);
        b_expr(ltrue_while, lfalse_while);
        match(')');
        code.emitLabel(ltrue_while);
        stat(lbegin);
        code.emit(OpCode.GOto, lbegin);
        break;

      default:
        error("[!] Error occured during execution of stat() with token: " + look);
        break;
    }
  }

  public void whenlist(int lnext) {
    switch (look.tag) {
      case Tag.WHEN:
        int lnext_when = code.newLabel();
        whenitem(lnext, lnext_when);
        code.emitLabel(lnext_when);
        whenlistp(lnext);
        break;

      default:
        error("[!] Error occured during execution of whenlist() with token: " + look);
        break;
    }
  }

  public void whenlistp(int lnext) {
    switch (look.tag) {
      case Tag.WHEN:
        int lnext_when = code.newLabel();
        whenitem(lnext, lnext_when);  // Se la prima b_expr() dovesse risultare vera, allora il prossimo blocco di codice è whenlistp.next, altrimenti è whenlistp1
        code.emitLabel(lnext_when);
        whenlistp(lnext);
        break;

      case Tag.ELSE:
        break;

      default:
        error("[!] Error occured during execution of whenlistp() with token: " + look);
        break;
    }
  }

  public void whenitem(int lnext, int lnext_f) {
    switch (look.tag) {
      case Tag.WHEN:
        match(Tag.WHEN);
        match('(');
        int b_true = code.newLabel();
        int b_false = lnext_f;
        b_expr(b_true, b_false);
        match(')');
        code.emitLabel(b_true);
        // s1_next = lnext;
        // stat(s1_next);
        stat(lnext);
        code.emit(OpCode.GOto, lnext);
        break;

      default:
        error("[!] Error occured during execution of whenitem() with token: " + look);
        break;
    }
  }

  private void b_expr(int ltrue, int lfalse) {
    switch (look.tag) {
      case '(':
      case Tag.NUM:
      case Tag.ID:
        expr();

        if (look == Word.eq) {
          match(Tag.RELOP);
          expr();
          code.emit(OpCode.if_icmpeq, ltrue);
        }
        else if (look == Word.ne) {
          match(Tag.RELOP);
          expr();
          code.emit(OpCode.if_icmpne, ltrue);
        }
        else if (look == Word.lt) {
          match(Tag.RELOP);
          expr();
          code.emit(OpCode.if_icmplt, ltrue);
        }
        else if (look == Word.gt) {
          match(Tag.RELOP);
          expr();
          code.emit(OpCode.if_icmpgt, ltrue);
        }
        else if (look == Word.le) {
          match(Tag.RELOP);
          expr();
          code.emit(OpCode.if_icmple, ltrue);
        }
        else if (look == Word.ge) {
          match(Tag.RELOP);
          expr();
          code.emit(OpCode.if_icmpge, ltrue);
        }
        else {
          error("[!] Syntax error: '" + look + "' is not a valid token");
        }

        code.emit(OpCode.GOto, lfalse);
        break;

      default:
        error("[!] Error occured during execution of b_expr() with token: " + look);
        break;
    }
  }

  private void expr() {
    switch (look.tag) {
      case '(':
      case Tag.NUM:
      case Tag.ID:
        term();
        exprp();
        break;

      default:
        error("[!] Error occured during execution of expr() with token: " + look);
        break;
    }
  }

  private void exprp() {
    switch(look.tag) {
      case '+':
        match('+');
        term();
        code.emit(OpCode.iadd);
        exprp();
        break;

      case '-':
        match('-');
        term();
        code.emit(OpCode.isub);
        exprp();
        break;

      case ';':
      case '}':
      case ')':
      case Tag.WHEN:
      case Tag.ELSE:
      case Tag.RELOP:
      case Tag.EOF:
        break;

      default:
        error("[!] Error occured during execution of exprp() with token: " + look);
        break;
    }
  }

  private void term() {
    switch (look.tag) {
      case '(':
      case Tag.NUM:
      case Tag.ID:
        fact();
        termp();
        break;

      default:
        error("[!] Error occured during execution of term() with token: " + look);
        break;
    }
  }

  private void termp() {
    switch (look.tag) {
      case '*':
        match('*');
        fact();
        code.emit(OpCode.imul);
        termp();
        break;

      case '/':
        match('/');
        fact();
        code.emit(OpCode.idiv);
        termp();
        break;

      case '+':
      case '-':
      case ';':
      case '}':
      case ')':
      case Tag.WHEN:
      case Tag.ELSE:
      case Tag.RELOP:
      case Tag.EOF:
        break;

      default:
        error("[!] Error occured during execution of termp() with token: " + look);
        break;
    }
  }

  private void fact() {
    switch (look.tag) {
      case '(':
        match('(');
        expr();
        match(')');
        break;

      case Tag.NUM:
        code.emit(OpCode.ldc, ((NumberTok) look).lexeme);
        match(Tag.NUM);
        break;

      case Tag.ID:
        if (st.lookupAddress(((Word) look).lexeme) != -1) {
          code.emit(OpCode.iload, st.lookupAddress(((Word) look).lexeme));
          match(Tag.ID);
          break;
        }
        else {
          error("[!] Identificator '" + ((Word) look).lexeme + "' has not been declared");
          break;
        }

      default:
        error("[!] Error occured during execution of fact() with token: " + look);
        break;
    }
  }

  public static void main(String[] args) {
    Lexer lexer = new Lexer();
    String path = args.length == 1 ? args[0] : "../tests/test.pas";

    try {
      BufferedReader br = new BufferedReader(new FileReader(path));
      Translator translator = new Translator(lexer, br);

      translator.prog();
      br.close();
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
