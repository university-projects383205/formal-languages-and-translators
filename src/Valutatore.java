import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Valutatore {
  private Lexer lex;
  private BufferedReader pbr;
  private Token look;

  public Valutatore(Lexer l, BufferedReader br) {
    lex = l;
    pbr = br;
    move();
  }

  private void move() {
    look = lex.lexical_scan(pbr);
    System.out.println("Token: " + look);
  }

  private void error(String s) {
    throw new Error("Near line " + lex.line + ": " + s);
  }

  private void match(int t) {
    if (look.tag == t) {
      if (look.tag != Tag.EOF) {
        move();
      }
    }
    else {
      error("[!] Syntax error: expected '" + (char) t + "', found " + look + " instead");
    }
  }

  public void start() {
    int expr_val = 0;

    switch (look.tag) {
      case '(':
      case Tag.NUM:
        expr_val = expr();
        match(Tag.EOF);
        System.out.println("[*] Evaluation: " + expr_val);
        break;

      default:
        error("[!] Error occured during execution of start()");
        break;
    }
  }

  private int expr() {
    int term_val = 0;
    int exprp_val = 0;

    switch (look.tag) {
      case '(':
      case Tag.NUM:
        term_val = term();
        exprp_val = exprp(term_val);
        break;

      default:
        error("[!] Error occured during execution of expr()");
        break;
    }

    return exprp_val;
  }

  private int exprp(int exprp_i) {
    int term_val = 0;
    int exprp_val = 0;

    switch (look.tag) {
      case '+':
        match('+');
        term_val = term();
        exprp_val = exprp(exprp_i + term_val);
        break;

      case '-':
        match('-');
        term_val = term();
        exprp_val = exprp(exprp_i - term_val);
        break;

      case ')':
      case Tag.EOF:
        exprp_val = exprp_i;
        break;

      default:
        error("[!] Error occured during execution of exprp()");
        break;
    }

    return exprp_val;
  }

  private int term() {
    int termp_i = 0;
    int term_val = 0;

    switch (look.tag) {
      case '(':
      case Tag.NUM:
        termp_i = fact();
        term_val = termp(termp_i);
        break;

      default:
        error("[!] Error occured during execution of term()");
        break;
    }

    return term_val;
  }

  private int termp(int termp_i) {
    int fact_val = 0;
    int termp_val = 0;

    switch (look.tag) {
      case '*':
        match('*');
        fact_val = fact();
        termp_val = termp(termp_i * fact_val);
        break;

      case '/':
        match('/');
        fact_val = fact();
        termp_val = termp(termp_i / fact_val);
        break;

      case '+':
      case '-':
      case ')':
      case Tag.EOF:
        termp_val = termp_i;
        break;

      default:
        error("[!] Error occured during execution of termp()");
        break;
    }

    return termp_val;
  }

  private int fact() {
    int expr_val = 0;
    int fact_val = 0;

    switch (look.tag) {
      case '(':
        match('(');
        expr_val = expr();
        match(')');
        fact_val = expr_val;
        break;

      case Tag.NUM:
        int num_value = ((NumberTok) look).lexeme;  // Downcast da Token a NumberTok, posso farlo perché mi trovo nel caso: look.tag == Tag.NUM
        match(Tag.NUM);
        fact_val = num_value;
        break;

      default:
        error("[!] Error occured during execution of fact()");
        break;
    }

    return fact_val;
  }

  public static void main(String[] args) {
    Lexer lex = new Lexer();
    String path = args[0];

    try {
      BufferedReader br = new BufferedReader(new FileReader(path));
      Valutatore valutatore = new Valutatore(lex, br);

      valutatore.start();
      br.close();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
