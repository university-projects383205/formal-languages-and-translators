/*
read(n);
i:=2;
f:=1;
while(i<=n) {
    f:=f*i;
    i:=i+1
};
print(f)
*/

read(n);
case when (n == 0) tot := 1 else tot := n;
i := n - 1;
while (i > 1) {
  tot:=tot*i;
  i:=i-1
};
print(tot)
